#include "../m_file.h"

/*
 * TEST 8 :
 * montre que l'on peut envoyer et recevoir autre chose que des char *
 */

int main(int argc, char **argv) {
    // ? CREATION DE LA FILE
    MESSAGE *file = m_connexion("/lala", O_CREAT | O_RDWR, 2, 200,
                                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    // MESSAGE *file = m_connexion("/lala", O_RDWR);

    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    int backup_stdout = dup(1);
    int devNull = open("/dev/null", O_WRONLY);
    if (devNull == -1) {
        printf("Couldn't open /dev/null'");
        exit(EXIT_FAILURE);
    }

    dup2(devNull, 1);

    // ? ENVOIE MESSAGE 1
    int t[2] = {-12, 99};
    struct mon_message *m = malloc(sizeof(struct mon_message) + sizeof(t));
    m->type = 122;
    memmove(m->mtext, t, sizeof(t));
    m->len = sizeof(t);
    m_envoi(file, m, sizeof(t), O_NONBLOCK);

    // ? RECEPTION MESSAGE 1
    int buff[2];
    if (m_reception(file, buff, 200, 122, O_NONBLOCK) == -1) {
        perror("m_reception");
        exit(EXIT_FAILURE);
    }

    dup2(backup_stdout, STDOUT_FILENO);
    printf("buff[1] : %d, buff[2] : %d\n", buff[0], buff[1]);

    return EXIT_SUCCESS;
}

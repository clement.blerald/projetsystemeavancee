# ProjetSystemeAvancee : File de message
## **Membres du groupes**

- Mébarki Clément : 71800676
- Blérald Clément : 71800092

## **Avancement du projet**

- Le projet est fonctionnel, toutes les fonctionnalités obligatoire on été implémentés. Il est possible :
    - D'**envoyer/recevoir** en accès concurrent ou pas
    - De **créer** une file et de s'y **connecter**
    - Les **accès en parallèle** sont sécurisé
#### **Choix d'implémentation notable**

- Nous avons choisis de ne pas utiliser de **tableaux circulaire**, la valeur de first est donc constante
- Pour gérer les possibles cases vides causés par une lecture en milieu de tableau, nous avons choisi de déplacer la mémoire (voir fonction **clean**)
- Nous utilisons **2 sémaphores** (écrivain/lecteur) pour gérer les accès parallèles
- implémentation de la file : l'entête se trouve dans le tableaux de message (au début)
## **Extensions**

### **Compacter les messages**

- Cette extension à été réalisé et est fonctionnelles, les indices first et last présent dans l'entête représentent des positions d'octets et non d'indice de tableau.  

- Celle-ci est vérifiable avec le test8. Pour le constater dans le code, on peut regarder l'implémentation des indices **first/last**.
### **Les signaux**

- MEBARKI => complète ici
## **Compilation**

- Pour ***compiler*** : à la **racine** ou dans le répertoire **Tests**

> make

## **Exécution du projet**

- Pour***compiler et exécuter*** un test:

```bash
cd Tests/
make test$i # changer $i par le numéro du test a lancer
```

## Architecture du Projet

```bash
.
├── Makefile
├── m_file.c
├── m_file.h
├── README.md
└── Tests
    ├── Makefile
    ├── test10.c
    ├── test1.c
    ├── test2.c
    ├── test3.c
    ├── test4.c
    ├── test5.c
    ├── test6.c
    ├── test7.c
    ├── test8.c
    └── test9.c
```
# **A TERMINER**

- Enlever les boucles while => utiliser des signaux **OK**
- tableau circulaire => mettre listes pairs d'indices (représentant les "cases vides" ?) 
- Réfléchir au parallélisme, "il n'y a pas de raisons qu'un lecteur bloque un écrivain et vice-versa"

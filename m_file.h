#ifndef M_FILE_H_
#define M_FILE_H_

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

#define SLEEP_TIME 1
#define MAX_REGISTERED 5
#define MAX_WAITERS 5

typedef struct mon_message {
    long type;
    size_t len;
    char mtext[];
} mon_message;

typedef struct mon_signal {
    long msg_type;
    int signal_code;
    pid_t pid;
} mon_signal;

typedef struct ENTETE {
    size_t max_msg_length;
    size_t min_msg_count;
    size_t nb_messages;
    size_t first;
    size_t last;
    sem_t sem_read;
    sem_t sem_write;
    mon_signal registered[MAX_REGISTERED];
    int registered_count;
    pid_t waiting_writers[MAX_WAITERS];
    pid_t waiting_readers[MAX_WAITERS];
    int waiting_writers_count;
    int waiting_readers_count;
} ENTETE;

typedef struct MESSAGE {
    long type;
    char *mem;
} MESSAGE;

MESSAGE *m_connexion(const char *nom, int options, ...);

int m_deconnexion(MESSAGE *file);

int m_destruction(const char *nom);

/**
 * @brief : envoie un message dans la file
 *
 * @param file : file de message
 * @param msg : pointeur vers le message à envoyer
 * @param len : taille du message (hors struct)
 * @param msgflag : option d'envoie (bloquant ou pas)
 * @return int : 1 = true, -1 = false
 */
int m_envoi(MESSAGE *file, const void *msg, size_t len, int msgflag);


/**
 * @brief permet de lire un message dans la file
 *
 * @param file file de message
 * @param msg pointeur vers mémoire ou on doit copier le message
 * @param len taille max que peut faire le message
 * @param type option de recherche
 * @param flags option de réception (bloquant ou pas)
 * @return ssize_t
 */
ssize_t m_reception(MESSAGE *file, void *msg, size_t len, long type, int flags);

// getters
size_t m_message_len(MESSAGE *);
size_t m_capacite(MESSAGE *);
size_t m_nb(MESSAGE *);

/**
 * @brief : enregistre un processus pour une file de message
 *
 * @param file : file de message
 * @param signal_code : le code du signal (0-31)
 * @param msg_type : le type à "écouter"
 * @return : -1 if error else 0
 */
int m_enregistrer(MESSAGE *file, int signal_code, long msg_type);

/**
 * @brief : désenregistre un processus pour une file de message
 *
 * @param file : file de message
 * @param pid : le pid du processus
 * @return : -1 if error else 0
 */
int m_desenregistrer(MESSAGE *file, pid_t pid);

#endif /* M_FILE_H_ */

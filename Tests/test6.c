#include "../m_file.h"

/*
 * TEST 6 : reception avec paramètre type < 0
 * 3 processus : les deux premier envoie 2 messages, un autre processus essaye
 * de lire un message d'un écrivain dont le pid < PID. variable PID : borne
 * inférieur du PID de l'écrivain dont on veut lire le message
 */

#define PID -1

int main(int argc, char **argv) {
    MESSAGE *file = m_connexion("/lala", O_CREAT | O_RDWR, 5, 20,
                                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    pid_t fils[3];

    if ((fils[0] = fork()) == 0) {  // fils 1 : écrivain n°1
        // MESSAGE 1
        char txt1[] = "message n°1";
        mon_message *msg1 = malloc(sizeof(mon_message) + sizeof(txt1));
        msg1->type = (long)getpid();

        memmove(msg1->mtext, txt1, sizeof(txt1));
        msg1->len = sizeof(txt1);

        if (m_envoi(file, msg1, sizeof(txt1), O_NONBLOCK) == -1) {
            printf("m_envoie: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        free(msg1);

    } else if ((fils[1] = fork()) == 0) {  // fils 2 : écrivain n°2
        // MESSAGE 2
        char txt2[] = "message n°2";
        mon_message *msg2 = malloc(sizeof(mon_message) + sizeof(txt2));
        msg2->type = (long)getpid();
        memmove(msg2->mtext, txt2, sizeof(txt2));
        msg2->len = sizeof(txt2);

        if (m_envoi(file, msg2, sizeof(txt2), O_NONBLOCK) == -1) {
            printf("m_envoie: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        free(msg2);
    } else if ((fils[2] = fork()) == 0) {  // fils 2 : lecteur
        sleep(2);
        char buff[20];

        if (m_reception(file, buff, sizeof(buff), PID, O_NONBLOCK) == -1) {
            printf("m_reception: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    while (wait(NULL) > 0)
        ;

    return EXIT_SUCCESS;
}

#include "../m_file.h"

/*
 * TEST 8 :
 * montre que l'on peut compacter des messages => en avoir plus que le
 * min_msg_count
 */

int main(int argc, char **argv) {
    // ? CREATION DE LA FILE

    MESSAGE *file = m_connexion("/lala", O_CREAT | O_RDWR, 2, 200,
                                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    // MESSAGE *file = m_connexion("/lala", O_RDWR);

    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    // ? ENVOIE MESSAGE 1
    char t[] = "Bonjour Clement\n";
    struct mon_message *m = malloc(sizeof(struct mon_message) + sizeof(t));
    m->type = 122;
    memmove(m->mtext, t, sizeof(t));
    m->len = sizeof(t);
    m_envoi(file, m, sizeof(t), O_NONBLOCK);

    // ? ENVOIE MESSAGE 2
    char t2[] = "Bonjour Mebarki\n";
    struct mon_message *m2 = malloc(sizeof(struct mon_message) + sizeof(t2));
    // m2->type = (long)getpid();
    m2->type = 123;
    memmove(m2->mtext, t2, sizeof(t2));
    m2->len = sizeof(t2);
    m_envoi(file, m2, sizeof(t2), O_NONBLOCK);

    // ? ENVOIE MESSAGE 3
    char t3[] = "Bonjour, je suis le troisième message\n";
    struct mon_message *m3 = malloc(sizeof(struct mon_message) + sizeof(t3));
    // m2->type = (long)getpid();
    m3->type = 123;
    memmove(m3->mtext, t3, sizeof(t3));
    m3->len = sizeof(t3);
    m_envoi(file, m3, sizeof(t3), O_NONBLOCK);

    // ? RECEPTION MESSAGE 1
    char buff[200];
    if (m_reception(file, buff, 200, 122, O_NONBLOCK) == -1) {
        perror("m_reception");
        exit(EXIT_FAILURE);
    }

    sleep(1);

    // ? RECEPTION MESSAGE 2
    char buff2[200];
    if (m_reception(file, buff2, 200, 123, O_NONBLOCK) == -1) {
        perror("m_reception");
        exit(EXIT_FAILURE);
    }

    sleep(1);
    // ? RECEPTION MESSAGE 3
    char buff3[200];
    if (m_reception(file, buff3, 200, 123, O_NONBLOCK) == -1) {
        perror("m_reception");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}

#include "../m_file.h"

/*
 * TEST 4 : envoie en mode bloquant
 * 2 processus : un envoie 1 message puis attends de pouvoir en envoyer un
 * autre, un autre processus lis deux messages.
 */

int main(int argc, char **argv) {
    printf("Je suis le père\n");
    MESSAGE *file = m_connexion("/lala", O_CREAT | O_RDWR, 1, 20,
                                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (fork() == 0) {  // fils 1 : écrivain
        // MESSAGE 1
        char txt1[] = "message n°1";
        mon_message *msg1 = malloc(sizeof(mon_message) + sizeof(txt1));
        msg1->type = (long)getpid();

        memmove(msg1->mtext, txt1, sizeof(txt1));
        msg1->len = sizeof(txt1);

        if (m_envoi(file, msg1, sizeof(txt1), 0) == -1) {
            printf("m_envoie: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        free(msg1);

        // MESSAGE 2
        char txt2[] = "message n°2";
        mon_message *msg2 = malloc(sizeof(mon_message) + sizeof(txt2));
        msg2->type = (long)getpid();
        memmove(msg2->mtext, txt2, sizeof(txt2));
        msg2->len = sizeof(txt2);

        if (m_envoi(file, msg2, sizeof(txt2), 0) == -1) {
            printf("m_envoie: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        free(msg2);

    } else if (fork() == 0) {  // fils 2 : lecteur
        sleep(2);
        char buff[20];

        if (m_reception(file, buff, sizeof(buff), 0, 0) == -1) {
            printf("m_reception: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        memset(buff, 0, sizeof(buff));

        if (m_reception(file, buff, sizeof(buff), 0, 0) == -1) {
            printf("m_reception: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    } 
    
    while (wait(NULL) > 0);
    
    return EXIT_SUCCESS;
}

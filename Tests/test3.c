#include "../m_file.h"

/*
 * TEST 3 : réception en mode bloquant
 * 2 processus se connecte à la file l'un veut recevoir un message et doit donc
 * se mettre en attente, l'autre sleep avant d'envoyer un message
 */

int main(int argc, char **argv) {
    MESSAGE *file = m_connexion("/lala", O_CREAT | O_RDWR, 10, 50,
                                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (fork() == 0) {  // fils 1
        char buff[20];
        m_reception(file, buff, 20, 0, 0);
    } else if (fork() == 0) {  // fils 2
        sleep(4);
        char txt[] = "ça marche bien";
        mon_message *msg = malloc(sizeof(mon_message) + sizeof(txt));
        msg->len = sizeof(txt);
        memmove(msg->mtext, txt, sizeof(txt));
        msg->type = (long)getpid();
        m_envoi(file, msg, 20, O_NONBLOCK);
        free(msg);
    }

    while (wait(NULL) > 0)
        ;

    return EXIT_SUCCESS;
}

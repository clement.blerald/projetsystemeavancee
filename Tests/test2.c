#include "../m_file.h"

/*
 * TEST 2 :
 * on se connecte à "/lala" sur 2 processus puis on se deconnecte avc 1, on test de recup m_nb de la liste dans lautre voir si ça marche
 */

int main(int argc, char **argv){

  MESSAGE *file;

  pid_t A_pid , B_pid;


  if ((A_pid = fork()) == 0) {
    file = m_connexion("/lala", O_RDWR);
    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    printf("fils A (%i) connecté\n", getpid());
    size_t l = m_capacite(file);
    printf("affichage de m_capacite à partir de A: %ld\n", l);

    if (m_deconnexion(file) == -1) {
  		printf("m_deconnexion: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
  	}

    printf("A se deconnecte\n");

    printf("LA\n");
    size_t l2 = m_capacite(file);
    printf("affichage de m_capacite à partir de A: %ld\n", l2);
  }
  else if ((B_pid = fork()) == 0) {
    file = m_connexion("/lala", O_RDWR);
    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    printf("fils B (%i) connecté\n", getpid());
    printf("fils B dort 1 seconde\n");
    sleep(1);
    size_t l = m_capacite(file);
    printf("affichage de m_capacite à partir de B: %ld\n", l);
  }
  else {
    printf("parent ici\n");
  }

  pid_t pid;
  while ((pid = wait(NULL)) > 0);

  return EXIT_SUCCESS;
}

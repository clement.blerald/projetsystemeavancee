#include <signal.h>

#include "../m_file.h"

#define SIGNAL_CODE 10
#define MSG_TYPE 123

MESSAGE *file;

void handler(int sig) {
    ENTETE *entete = (ENTETE *)file->mem;
    char buff[entete->max_msg_length];
    if (m_reception(file, buff, entete->max_msg_length, MSG_TYPE, O_NONBLOCK) ==
        -1) {
        perror("m_reception");
        exit(EXIT_FAILURE);
    }
    printf("(%s)\n", buff);

    m_desenregistrer(file, getpid());
    exit(0);
}

/*
 * TEST 7 :
 * enregistre un processus sur une file, à tester avc test 10
 */
int main(int argc, char **argv) {
    // file = m_connexion("/lala", O_CREAT | O_RDWR, 10, 50, S_IRUSR | S_IWUSR |
    // S_IRGRP | S_IWGRP);
    file = m_connexion("/lala", O_RDWR);
    if (file == NULL) {
        printf("m_connexion: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    struct sigaction act;
    act.sa_handler = &handler;
    sigfillset(&act.sa_mask);   // ?
    act.sa_flags = SA_RESTART;  // ?
    if (sigaction(SIGNAL_CODE, &act, NULL) == -1) {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    if (m_enregistrer(file, SIGNAL_CODE, MSG_TYPE) == -1) {
        printf("LA\n");
    }

    ENTETE *entete = (ENTETE *)file->mem;
    printf("enregistrés count: %d\n", entete->registered_count);

    printf("pid: %d\n", getpid());

    while (pause())
        ;
    return EXIT_SUCCESS;
}

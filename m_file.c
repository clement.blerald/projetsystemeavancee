#include "m_file.h"

MESSAGE *m_connexion(const char *nom, int options, ...) {
    int fd;
    char *mem;

    if ((options & O_CREAT) > 0 || nom == NULL) {
        printf("on create\n");
        va_list ap;
        va_start(ap, options);
        size_t nb_msg = va_arg(ap, size_t);
        size_t len_max = va_arg(ap, size_t);
        mode_t mode = va_arg(ap, mode_t);
        va_end(ap);

        sem_t sem_read;
        sem_init(&sem_read, 0, 1);

        sem_t sem_write;
        sem_init(&sem_write, 0, 1);

        // mon_signal registered[MAX_REGISTERED];

        ENTETE entete = {
            len_max,   nb_msg, 0, sizeof(ENTETE), sizeof(ENTETE), sem_read,
            sem_write, {},     0};

        size_t tot_size =
            sizeof(ENTETE) + nb_msg * (len_max + sizeof(struct mon_message));

        if (nom == NULL) {
            printf("on create mmap anonyme\n");
            mem = mmap(NULL, tot_size, PROT_READ | PROT_WRITE,
                       MAP_SHARED | MAP_ANONYMOUS, -1, 0);
            if (mem == MAP_FAILED) {
                perror("mmap");
                return NULL;
            }
        } else {
            printf("on create mmap nommé avc shm\n");
            fd = shm_open(nom, O_CREAT | O_RDWR, mode);
            if (fd == -1) {
                perror("shm_open");
                return NULL;
            }

            if (ftruncate(fd, tot_size) == -1) {
                perror("ftruncate");
                return NULL;
            }

            mem =
                mmap(NULL, tot_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
            if (mem == MAP_FAILED) {
                perror("mmap");
                return NULL;
            }
        }

        printf("on met l'entete dans le mmap\n");
        memcpy(mem, &entete, sizeof(ENTETE));  // j'ai changé la pour test
    } else {
        printf("on se connecte à un shm nommé\n");
        fd = shm_open(nom, O_RDWR, S_IWUSR | S_IRUSR);
        if (fd == -1) {
            perror("shm_open");
            return NULL;
        }

        struct stat sb;
        if (fstat(fd, &sb) == -1) {
            perror("fstat");
            return NULL;
        }

        mem = mmap(NULL, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (mem == MAP_FAILED) {
            perror("mmap");
            return NULL;
        }
    }

    printf("on crée la struct file\n");
    MESSAGE *file = malloc(sizeof(MESSAGE));

    int type = 0;
    if ((options & O_RDWR) > 0)
        type = O_RDWR;
    else if ((options & O_RDONLY) > 0)
        type = O_RDONLY;
    else if ((options & O_WRONLY) > 0)
        type = O_WRONLY;
    file->type = type;
    file->mem = mem;

    return file;
}

int m_enregistrer(MESSAGE *file, int signal_code, long msg_type) {
    pid_t pid = getpid();
    mon_signal signal = {msg_type, signal_code, pid};

    ENTETE *entete = (ENTETE *)file->mem;
    if (entete->registered_count >= MAX_REGISTERED) {
        printf("trop de gens déjà enregistrés");
        return -1;
    }

    entete->registered[entete->registered_count++] = signal;
    return 0;
}

int m_desenregistrer(MESSAGE *file, pid_t pid) {
    ENTETE *entete = (ENTETE *)file->mem;
    unsigned pos = -1;
    for (unsigned i = 0; i < entete->registered_count; i++) {
        mon_signal mon_signal_ = entete->registered[i];
        if (mon_signal_.pid == pid) pos = i;
    }

    if (pos == -1) {
        printf("on veut supprimer un pid qui existe pas\n");
        return -1;
    }
    // AAAVV pos = 1
    // AAVV?
    printf(
        "on veut supprimer le pid à l'indexe %d, donc deplacer %d vers la "
        "gauche\n",
        pos, MAX_REGISTERED - pos - 1);
    if (pos < MAX_REGISTERED - 1)  // pos [0, 3], 0: 3, 1: 2, 2: 1, 3: 0
        memcpy(&entete->registered[pos], &entete->registered[pos + 1],
               sizeof(mon_signal) * (MAX_REGISTERED - pos - 1));

    entete->registered_count--;
    return 0;
}

void envoyer_signaux(MESSAGE *file, mon_message *msg) {
    ENTETE *entete = (ENTETE *)file->mem;

    printf("reg count: %d\n", entete->registered_count);
    for (unsigned i = 0; i < entete->registered_count; i++) {
        mon_signal mon_signal_ = entete->registered[i];
        if (mon_signal_.msg_type == msg->type) {
            printf("on envoi à pid: %d, signal code: %d pour le type: %ld\n",
                   mon_signal_.pid, mon_signal_.signal_code,
                   mon_signal_.msg_type);
            if (kill(mon_signal_.pid, mon_signal_.signal_code) == -1) {
                perror("kill");
                m_desenregistrer(file, mon_signal_.pid);
            }
            return;
        }
    }

    if (entete->waiting_readers_count > 0) {
        if (kill(entete->waiting_readers[entete->waiting_readers_count - 1],
                 SIGUSR1) == -1) {
            perror("kill\n");
        }
        printf("kill : envoie du signal à l écrivain : %d\n",
               entete->waiting_readers[entete->waiting_readers_count - 1]);
        entete->waiting_readers_count -= 1;
    }
}

int m_deconnexion(MESSAGE *file) {
    ENTETE *entete = (ENTETE *)file->mem;
    size_t tot_size = entete->min_msg_count *
                      (entete->max_msg_length + sizeof(struct mon_message));
    if (munmap(file->mem, tot_size) == -1) {
        perror("munmap");
        return -1;
    }
    return 0;
}

int m_destruction(const char *nom) {
    if (shm_unlink(nom) == -1) {
        perror("shm_unlink");
        return -1;
    }
    return 0;
}

void afficher_message(struct mon_message *m_msg, int envoie) {
    if (envoie == 1)
        printf(
            "\033[1;36mMessage envoyé\033[0m | \033[1;31mexpéditeur\033[0m "
            ": %ld, \033[1;33mtaille\033[0m : %ld, "
            "\033[1;32mmessage\033[0m : %s\n",
            m_msg->type, m_msg->len, m_msg->mtext);
    else
        printf(
            "\033[1;37mMessage reçu \033[0m | \033[1;31mexpéditeur "
            "\033[0m : %ld, \033[1;32m message \033[0m : %s\n",
            m_msg->type, m_msg->mtext);
}

void empty_handler(int sig) {}

int m_envoi(MESSAGE *file, const void *msg, size_t len, int msgflag) {
    if (file->type != O_RDWR && file->type == O_WRONLY) {
        printf("Vous n'avez pas les droits en écriture sur cette file");
        return -1;
    }

    ENTETE *entete = (ENTETE *)file->mem;

    if (len > entete->max_msg_length) {
        printf("taille de message non autorisé\n");
        return -1;
    }

    int tot_size = entete->min_msg_count *
                       (entete->max_msg_length + sizeof(struct mon_message)) +
                   sizeof(ENTETE);
    mon_message *m_msg = (mon_message *)msg;

    while (sizeof(struct mon_message) + len + entete->last >
           tot_size) {  // file pleine
        if (msgflag == O_NONBLOCK) {
            printf("file pleine\n");
            errno = EAGAIN;
            return -1;
        } else if (msgflag == 0) {
            struct sigaction act;
            act.sa_handler = empty_handler;
            sigaction(SIGUSR1, &act, NULL);

            if (entete->waiting_writers_count < MAX_WAITERS) {
                entete->waiting_writers[entete->waiting_writers_count] =
                    getpid();
                entete->waiting_writers_count += 1;
            } else {
                perror(
                    "Il y a déjà trop d'écrivain en attente, veuillez "
                    "réessayer plus tard\n");
                return -1;
            }

            printf("je sleep dans envoie\n");
            pause();
            printf("je me réveille dans envoie\n");

        } else {
            printf("flag inexistant : %d\n", msgflag);
            return -1;
        }
    }

    // on ajoute le message
    int old_last = entete->last;

    sem_wait(&entete->sem_write);
    entete->last += sizeof(struct mon_message) + len;
    sem_post(&entete->sem_write);

    memset(file->mem + old_last, 0, sizeof(struct mon_message) + len);
    memcpy(file->mem + old_last, m_msg, sizeof(struct mon_message) + len);

    entete->nb_messages += 1;
    afficher_message(m_msg, 1);

    envoyer_signaux(file, m_msg);
    return 1;
}

/**
 * @brief : déplace la mémoire après une lecture
 *
 * @param file : file de message
 * @param entete : entete de la file
 * @param start : indice du début du message que l'on a lu
 * @param len : taille totale de la struct stockant ce message + du message
 * @return void
 */
void clean(MESSAGE *file, ENTETE *entete, int start, int len) {
    int tot_size = entete->min_msg_count *
                   (entete->max_msg_length + sizeof(struct mon_message));

    sem_wait(&entete->sem_write);

    memset(file->mem + start, 0, len);
    if (memmove(file->mem + start, file->mem + start + len, tot_size - len) ==
        NULL) {
        perror("memmove");
        exit(EXIT_FAILURE);
    }

    sem_post(&entete->sem_write);

    entete->nb_messages -= 1;
    entete->last -= len;
}

/**
 * @brief : parcours la liste de message est renvoie un message
 *
 * @param file : file de message
 * @param msg : pointeur vers mémoire ou on doit copier le message
 * @param type : option de recherche
 * @param entete : entete de la file
 * @param m_msg : message que l'on a lu dans la file
 * @return int : pid de l'écrivain du message que l'on a lu
 */
int parcoursMessage(MESSAGE *file, void *msg, long type, ENTETE *entete,
                    mon_message *m_msg) {
    size_t pos = entete->first;

    for (int i = 0; i < m_nb(file); i++) {
        memcpy(m_msg, file->mem + pos, sizeof(struct mon_message));
        memcpy(m_msg, file->mem + pos, sizeof(struct mon_message) + m_msg->len);

        if ((type > 0 && m_msg->type == type) ||
            (type < 0 && m_msg->type <= abs(type))) {
            memcpy(msg, m_msg->mtext, m_msg->len);
            return m_msg->type;
        }

        pos += sizeof(struct mon_message) + m_msg->len;
    }

    return -1;
}

ssize_t m_reception(MESSAGE *file, void *msg, size_t len, long type,
                    int flags) {
    if (file->type != O_RDWR && file->type == O_RDONLY) {
        printf("Vous n'avez pas les droits en lecture sur cette file");
        return -1;
    }

    ENTETE *entete = (ENTETE *)file->mem;

    mon_message *m_msg = malloc(sizeof(struct mon_message) + len);
    memset(m_msg, 0, sizeof(struct mon_message) + len);

    while (m_nb(file) == 0) {  // file vide
        if (flags == O_NONBLOCK) {
            printf("file vide\n");
            errno = EAGAIN;
            return -1;
        } else if (flags == 0) {
            struct sigaction act;
            act.sa_handler = empty_handler;
            sigaction(SIGUSR1, &act, NULL);

            if (entete->waiting_readers_count < MAX_WAITERS) {
                entete->waiting_readers[entete->waiting_readers_count] =
                    getpid();
                entete->waiting_readers_count += 1;
            } else {
                perror(
                    "Il y a déjà trop de lecteur en attente, veuillez "
                    "réessayer plus "
                    "tard\n");
                return -1;
            }

            printf("je sleep dans réception\n");
            pause();
            printf("je me réveille dans réception\n");

        } else {
            printf("flag inexistant : %d\n", flags);
            return -1;
        }
    }

    // si il y a des messages dans la file
    memcpy(m_msg, file->mem + entete->first, sizeof(struct mon_message));
    if (m_msg->len > len) {
        printf("message trop grand : msg->len : %ld, len : %ld\n", m_msg->len,
               len);
        errno = EMSGSIZE;
        return -1;
    }

    // ? option de recherche
    sem_wait(&entete->sem_read);
    if (type == 0) {
        memcpy(m_msg, file->mem + entete->first,
               sizeof(struct mon_message) + m_msg->len);

        memcpy(msg, m_msg->mtext, m_msg->len);
        afficher_message(m_msg, 0);
    } else {  // on cherche un écrivain en particulier
        if (parcoursMessage(file, msg, type, entete, m_msg) == -1) {
            if (type > 0)
                printf("Il n'y a pas de message de l'expediteur : %ld\n", type);
            else
                printf(
                    "Il n'y a pas de message d'expéditeur avec un pid "
                    "inférieur à : %ld\n",
                    -type);
            sem_post(&entete->sem_read);
        } else {
            afficher_message(m_msg, 0);
        }
    }
    clean(file, entete, entete->first, sizeof(struct mon_message) + m_msg->len);
    sem_post(&entete->sem_read);

    struct sigaction act;
    act.sa_handler = empty_handler;
    sigaction(SIGUSR1, &act, NULL);

    if (entete->waiting_writers_count > 0) {
        if (kill(entete->waiting_writers[entete->waiting_writers_count - 1],
                 SIGUSR1) == -1) {
            perror("kill\n");
        }
        printf("kill : envoie du signal à l écrivain : %d\n",
               entete->waiting_writers[entete->waiting_writers_count - 1]);
        entete->waiting_writers_count -= 1;
    }

    free(m_msg);
    return 1;
}

/**
 * @brief : getter sur la taille maximum d'un message
 *
 * @param file : file de message
 * @return size_t
 */
size_t m_message_len(MESSAGE *file) {
    ENTETE *entete = (ENTETE *)file->mem;
    return entete->max_msg_length;
}

/**
 * @brief : getter sur le minimum de message que la file peut stocker
 *
 * @param file : file de message
 * @return size_t
 */
size_t m_capacite(MESSAGE *file) {
    ENTETE *entete = (ENTETE *)file->mem;
    return entete->min_msg_count;
}

/**
 * @brief : getter sur le nombre de message dans la file
 *
 * @param file : file de message
 * @return size_t
 */
size_t m_nb(MESSAGE *file) {
    ENTETE *entete = (ENTETE *)file->mem;
    return entete->nb_messages;
}

ALL = tests
all:$(ALL)

tests:
	cd Tests && make

clean:
	rm -rf $(ALL) *.o a.out
	cd Tests && make clean
